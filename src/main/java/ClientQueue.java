import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientQueue implements Runnable{
    private boolean isEmpty=true;
    private ArrayBlockingQueue<Client> waitingClients;
    private AtomicInteger waitingTime;
    private AtomicInteger clientsServed;
    private AtomicInteger totalWaitingTime;
    private AtomicInteger totalServiceTime;

    public ClientQueue(int nrOfClients){
        waitingClients=new ArrayBlockingQueue<>(nrOfClients);
        waitingTime=new AtomicInteger(0);
        clientsServed=new AtomicInteger(0);
        totalWaitingTime=new AtomicInteger(0);
        totalServiceTime=new AtomicInteger(0);
    }

    public void addClient(Client client){
        waitingTime.set(waitingTime.get()+client.getServiceTime());
        waitingClients.add(client);
        isEmpty=false;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!isEmpty){
                for(Client client: waitingClients){
                    client.incrementWaitingTime();
                }
                waitingClients.element().setServiceTime(waitingClients.element().getServiceTime()-1);
                waitingTime.set(waitingTime.get()-1);
                if(waitingClients.element().getServiceTime()==0){
                    clientsServed.set(clientsServed.get()+1);
                    totalWaitingTime.set(totalWaitingTime.get()+waitingClients.element().getWaitingTime());
                    totalServiceTime.set(totalServiceTime.get()+waitingClients.element().getInitialServiceTime());
                    waitingClients.remove();
                    if(waitingClients.size()==0){
                        isEmpty=true;
                        waitingTime.set(0);
                    }

                }
            }
        }
    }

    public int getWaitingTime() {
        return waitingTime.get();
    }


    public ArrayBlockingQueue<Client> getWaitingClients() {
        return waitingClients;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public int getTotalWaitingTime() {
        return totalWaitingTime.get();
    }

    public int getClientsServed() {
        return clientsServed.get();
    }

    public int getTotalServiceTime() {
        return totalServiceTime.get();
    }
}
