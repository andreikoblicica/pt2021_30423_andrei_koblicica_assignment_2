
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import static java.lang.Thread.sleep;

public class SimulationManager implements Runnable{

    public int simulationTime;
    public int currentTime=0;

    private int nrOfClients;
    private int nrOfQueues;
    private boolean canPrint=false;
    private boolean isFinished=false;

    private Random random=new Random();
    private FileWriter fileWriter;
    private DecimalFormat decimalFormat=new DecimalFormat("#.##");

    private ArrayList<Client> clients=new ArrayList<>();
    private ArrayList<ClientQueue> queues=new ArrayList<>();
    private ArrayList<Thread> queueThreads=new ArrayList<>();

    private int peakTime=0;
    private int maxClients=0;
    private double averageWaitingTime=0;
    private double averageServingTime=0;

    public SimulationManager(int simulationTime, int nrOfClients, int nrOfQueues, int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime) {
        this.simulationTime = simulationTime;
        this.nrOfClients = nrOfClients;
        this.nrOfQueues = nrOfQueues;
        generateRandomClients(minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime);
        generateQueues();
        try{
            fileWriter=new FileWriter("D:\\PT2021\\pt2021_30423_andrei_koblicica_assignment_2\\log.txt",true);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public void run(){
        for(Thread thread: queueThreads){
            thread.start();
        }
        while(currentTime<=simulationTime){
            addClientsToQueues();
            printToFile();
            calculatePeakTime();
            canPrint=true;
            if(clients.size()==0 && getNrOfClientsInQueues()==0){
                break;
            }
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            currentTime++;
        }
        printStats();
        isFinished=true;
        try {
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void generateRandomClients(int minArrivalTime,int maxArrivalTime,int minServiceTime, int maxServiceTime){
        for(int i=0;i<nrOfClients;i++){
            int arrivalTime=minArrivalTime+ random.nextInt(maxArrivalTime-minArrivalTime+1);
            int serviceTime=minServiceTime+ random.nextInt(maxServiceTime-minServiceTime+1);
            Client client=new Client(i+1,arrivalTime,serviceTime);
            clients.add(client);
        }
        sortClients();
    }

    private void sortClients(){
        Collections.sort(clients, new Comparator<Client>(){
            public int compare(Client client1, Client client2){
                if(client1.getArrivalTime() == client2.getArrivalTime()){
                    if(client1.getServiceTime()==client2.getServiceTime()){
                        return 0;
                    }
                    return client1.getServiceTime() > client2.getServiceTime() ? -1 : 1;
                }
                return client1.getArrivalTime() < client2.getArrivalTime() ? -1 : 1;
            }
        });
    }

    private void generateQueues(){
        for(int i=0;i<nrOfQueues;i++){
            ClientQueue queue=new ClientQueue(nrOfClients);
            queues.add(queue);
            queueThreads.add(new Thread(queue));
            queueThreads.get(i).setDaemon(true);
        }

    }
    private int getBestQueue(){
        int queueIndex=0;
        int minTime=queues.get(0).getWaitingTime();
        for(int i=1;i<nrOfQueues;i++){
            if(queues.get(i).getWaitingTime()<minTime){
                minTime=queues.get(i).getWaitingTime();
                queueIndex=i;
            }
        }
        return queueIndex;
    }
    private int getNrOfClientsInQueues(){
        int currentClients=0;
        for(ClientQueue queue: queues) {
            currentClients+=queue.getWaitingClients().size();
        }
        return currentClients;
    }

    private void addClientsToQueues() {
        int deleteFirst=0;
        for(Client client: clients){
            if(client.getArrivalTime()==currentTime){
                int bestQueue=getBestQueue();
                queues.get(bestQueue).addClient(client);
                deleteFirst++;
            }
            else{
                break;
            }
        }
        for(int i=0;i<deleteFirst;i++){
            clients.remove(0);
        }
    }

    private void calculatePeakTime(){
        int currentClients=getNrOfClientsInQueues();
        if(currentClients>maxClients){
            maxClients=currentClients;
            peakTime=currentTime;
        }
    }

    private void printToFile() {
        try {
            fileWriter.write("Time " + currentTime + "\n");
            fileWriter.write("Waiting clients: ");
            if(clients.size()==0){
                fileWriter.write("none");
            }
            else{
                for(Client client: clients){
                    fileWriter.write("("+client.getId()+","+client.getArrivalTime()+","+client.getServiceTime()+"); ");
                }
            }
            for(int i=0;i<queues.size();i++){
                fileWriter.write("\nQueue "+(i+1)+": ");
                if(queues.get(i).isEmpty()){
                    fileWriter.write("closed");
                }
                else {
                    for (Client client : queues.get(i).getWaitingClients()) {
                        fileWriter.write("(" + client.getId() + "," + client.getArrivalTime() + "," + client.getServiceTime() + "); ");
                    }
                }
            }
            fileWriter.write("\n\n");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    private void printStats(){
        try {
            double totalWaitingTime=0;
            double totalServiceTime=0;
            int totalClientsServed=0;

            for(ClientQueue queue: queues){
                totalWaitingTime+=queue.getTotalWaitingTime();
                totalServiceTime+=queue.getTotalServiceTime();
                totalClientsServed+=queue.getClientsServed();
            }
            if(totalClientsServed>0){
                averageWaitingTime=totalWaitingTime/totalClientsServed;
                averageServingTime=totalServiceTime/totalClientsServed;
            }
            fileWriter.write("Simulation statistics:\n");
            fileWriter.write("Average waiting time is "+decimalFormat.format(averageWaitingTime)+"\n");
            fileWriter.write("Average serving time is "+decimalFormat.format(averageServingTime)+"\n");
            fileWriter.write("Peak time is "+peakTime+"\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Client> getClients() {
        return clients;
    }

    public ArrayList<ClientQueue> getQueues() {
        return queues;
    }

    public int getPeakTime() {
        return peakTime;
    }

    public double getAverageWaitingTime() {
        return averageWaitingTime;
    }

    public double getAverageServingTime() {
        return averageServingTime;
    }

    public boolean canPrint() {
        return canPrint;
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    public boolean isFinished() {
        return isFinished;
    }
}
