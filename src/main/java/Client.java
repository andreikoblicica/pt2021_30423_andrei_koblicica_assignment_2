public class Client {
    private int id;
    private int arrivalTime;
    private int serviceTime;
    private int initialServiceTime;
    private int waitingTime;
    public Client(int id, int arrivalTime, int serviceTime){
        this.id=id;
        this.arrivalTime=arrivalTime;
        this.serviceTime=serviceTime;
        this.initialServiceTime=serviceTime;
        waitingTime=0;
    }

    public int getId() {
        return id;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public int getInitialServiceTime() {
        return initialServiceTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void incrementWaitingTime(){
        waitingTime++;
    }
}
