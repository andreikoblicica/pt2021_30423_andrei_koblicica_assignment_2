import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.text.DecimalFormat;


public class Controller{

    private SimulationManager simulationManager;
    Thread simulationThread;
    private Timeline timeline;
    DecimalFormat decimalFormat=new DecimalFormat("#.##");

    @FXML
    private TextField nrOfClientsField;
    @FXML
    private TextField nrOfQueuesField;
    @FXML
    private TextField simulationTimeField;
    @FXML
    private TextField minArrivalTimeField;
    @FXML
    private TextField maxArrivalTimeField;
    @FXML
    private TextField minServiceTimeField;
    @FXML
    private TextField maxServiceTimeField;

    @FXML
    private Button clearButton;
    @FXML
    private Button startSimulationButton;

    @FXML
    private Label timeLabel=new Label();

    @FXML
    private Label averageWaitingLabel;
    @FXML
    private Label averageServingLabel;
    @FXML
    private Label peakTimeLabel;
    @FXML
    private Label resultsLabel;

    @FXML
    private TextArea logsArea;

    @FXML
    private void pressClear(ActionEvent event){
        timeline.stop();
        nrOfClientsField.clear();
        nrOfQueuesField.clear();
        simulationTimeField.clear();
        minArrivalTimeField.clear();
        maxArrivalTimeField.clear();
        minServiceTimeField.clear();
        maxServiceTimeField.clear();
        logsArea.clear();
        resultsLabel.setText("");
        averageWaitingLabel.setText("");
        averageServingLabel.setText("");
        peakTimeLabel.setText("");
        timeLabel.setText("");
        simulationThread.stop();
    }
    @FXML
    private void pressStartSimulation(ActionEvent event) throws Exception {
        if(validateInput()){
            simulationThread=new Thread(simulationManager);
            simulationThread.start();
            refreshInterface();
        }
        else{
            loadAlertBox("Incorrect simulation parameters!");
        }
    }

    private void refreshInterface() {
        timeline=new Timeline(new KeyFrame(Duration.millis(10),e->{
            if(simulationManager.canPrint()){
                timeLabel.setText("Current time: "+simulationManager.currentTime);
                logsArea.appendText("Time " + simulationManager.currentTime + "\n");
                logsArea.appendText("Waiting clients: ");
                if(simulationManager.getClients().size()==0){
                    logsArea.appendText("none");
                }
                else{
                    for(Client client: simulationManager.getClients()){
                        logsArea.appendText("("+client.getId()+","+client.getArrivalTime()+","+client.getServiceTime()+"); ");
                    }
                }
                for(int i=0;i<simulationManager.getQueues().size();i++){
                    logsArea.appendText("\nQueue "+(i+1)+": ");
                    if(simulationManager.getQueues().get(i).isEmpty()){
                        logsArea.appendText("closed");
                    }
                    else {
                        for (Client client : simulationManager.getQueues().get(i).getWaitingClients()) {
                            logsArea.appendText("(" + client.getId() + "," + client.getArrivalTime() + "," + client.getServiceTime() + "); ");
                        }
                    }
                }
                logsArea.appendText("\n\n");
                simulationManager.setCanPrint(false);
            }
            if(simulationManager.isFinished()){
                resultsLabel.setText("Results:");
                averageWaitingLabel.setText("Average waiting time: "+ decimalFormat.format(simulationManager.getAverageWaitingTime()));
                averageServingLabel.setText("Average serving time: "+decimalFormat.format(simulationManager.getAverageServingTime()));
                peakTimeLabel.setText("Peak time: "+simulationManager.getPeakTime());
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }


    private boolean validateInput(){
        try {
            int nrOfClients = Integer.parseInt(nrOfClientsField.getText());
            int nrOfQueues = Integer.parseInt(nrOfQueuesField.getText());
            int simulationTime = Integer.parseInt(simulationTimeField.getText());
            int minArrivalTime = Integer.parseInt(minArrivalTimeField.getText());
            int maxArrivalTime = Integer.parseInt(maxArrivalTimeField.getText());
            int minServiceTime = Integer.parseInt(minServiceTimeField.getText());
            int maxServiceTime = Integer.parseInt(maxServiceTimeField.getText());
            if(nrOfClients>0 && nrOfQueues>0 && simulationTime>0 && minArrivalTime>0 && maxArrivalTime>0 && minServiceTime>0 && maxServiceTime>0 && minArrivalTime<=maxArrivalTime && minServiceTime<=maxServiceTime){
                simulationManager=new SimulationManager(simulationTime,nrOfClients,nrOfQueues,minArrivalTime,maxArrivalTime,minServiceTime,maxServiceTime);
                return true;
            }
        }catch(NumberFormatException e){
            return false;
        }
        return false;
    }


    public void loadAlertBox(String message)throws Exception{
        Stage stage=new Stage(); //need a new window
        stage.setTitle("Error");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL); //can only do something else if this is taken care of

        FXMLLoader loader=new FXMLLoader(getClass().getResource("AlertBox.fxml"));
        Parent root = loader.load();

        AlertBoxController alertBoxController=loader.getController();
        alertBoxController.initializeLabel(message); //pass error message to be displayed

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
    }



}
